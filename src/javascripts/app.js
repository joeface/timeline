/*
Copyright Current Time, 2018
Written by Mikhail Ageev / michaelageev.com
*/

// import Hammer from './hammer.js';

import './modules'
const bootstrap = require('bootstrap')

if (PRODUCTION) {
  console.log("PRODUCTION");
} else {
  console.log(
    "%c DEVELOPMENT!",
    "color: orange; font-weight: bold; font-size: 36px; font-family: Skolar Sans Latin"
  );
}

var d = function (a) {
  try {
    console.log(a)
  } catch (e) {

  }
}

$(document).ready(function () {

  d('Init Timeline');

  $('[data-toggle="tooltip"]').tooltip();
  
  $('#toggle-filters').bind('click', function(){
    var action = $('#filters').is(':visible') ? 'hide': 'show';
    $("#filters").fadeToggle('show' == action?300:200);
    $('a', this).text($('a', this).data(action));
    return false;
  });

  var toggleYearHeadings = function(){
    $('.year').each(function () {
      if ($('li[data-visible=1]', this).length == 0) {
        $('h2', this).hide();
      }
    });
  }

  $('#filters input').bind('click', function(){
    
   $('#timeline').fadeOut(200, function(){
      $('#timeline li').show().attr('data-visible', '1');
      $('#timeline .year h2').show();

     // Hide disable countries
     if ($('#filters .checkboxes[data-filter="country"] input:checked').length != 2) {
       var country = $('#filters .checkboxes[data-filter="country"] input:checked').parent().data('filter');
       $('#timeline li:not([data-country=' + country + '])').removeAttr('data-visible').hide();
     }

     // Hide disabled reasons
     if ($('#filters .checkboxes[data-filter="reason"] input:checked').length != 4) {
       $('#filters .checkboxes[data-filter="reason"] input:not(:checked)').each(function () {
         $('#timeline li[data-reason=' + $(this).parent().data('filter') + ']').removeAttr('data-visible').hide();
       });
     }

     // Hide records by sanctions type
     $('#filters .checkboxes[data-filter="type"] input:not(:checked)').each(function () {
       $('#timeline li[data-' + $(this).parent().data('filter') + '=1]').removeAttr('data-visible').hide();
     });

     toggleYearHeadings();

     $('#timeline').fadeIn(300, function(){
       
     });

   });

    
  });

});