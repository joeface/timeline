export default class Progress {
  constructor(el) {
    
    function updateProgressBar() {
      let windowScrollTop = $(window).scrollTop()
      let windowHeight = $(window).outerHeight()
      let bodyHeight = $(document).height()

      let total = windowScrollTop / (bodyHeight - windowHeight) * 100

      $(".bar").css("width", total + "%")
    }

    function throttle(fn, wait) {
      let time = Date.now()
      return function() {
        if (time + wait - Date.now() < 0) {
          fn();
          time = Date.now()
        }
      }
    }

    $(window).scroll(throttle(updateProgressBar, 25))
  }
}