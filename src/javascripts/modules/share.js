export default class Share {
  constructor(el) {

    let thisUrl = window.location.href

    let $nav = $(el),
      $shareButton = $nav.find('#share-wrapper'),
      $drawer = $nav.find('#share-drawer'),
      $fb = $nav.find('#fb-share'),
      $tw = $nav.find('#twitter-share'),
      $reddit = $nav.find('#reddit-share'),
      $vk = $nav.find('#vk-share'),
      $telegram = $nav.find('#telegram-share'),
      description = $('meta[property="og:description"]').attr('content')

    $shareButton.click((el) => {
      $shareButton.toggleClass('opened').toggleClass('closed')
    })
    
    $fb.attr('href', `https://www.facebook.com/sharer/sharer.php?u=${thisUrl}`)
    $tw.attr('href', `https://twitter.com/home?status=${encodeURIComponent(description)} ${thisUrl}`)
    $vk.attr('href', `http://vk.com/share.php?url=${thisUrl}`)
    $reddit.attr('href', `http://www.reddit.com/submit?url=${thisUrl}&title=${encodeURIComponent(description)}`)
    $telegram.attr('href', `https://t.me/share/url?url=${encodeURIComponent(thisUrl)}`)

    $(document).click(function(event) { 
      if(!$(event.target).closest($shareButton).length) {
          if($($drawer).is(":visible")) {
            closeDrawer()
          }
      }        
    })

    function throttle(fn, wait) {
      let time = Date.now()
      return function() {
        if (time + wait - Date.now() < 0) {
          fn();
          time = Date.now()
        }
      }
    }

    function closeDrawer() {
      if ($shareButton.hasClass('opened')){
        $shareButton.removeClass("opened").addClass("closed")
      }
    }

    // ** TODO **
    // MAKE THROTTLE FUNCTION AVAILABLE TO ALL MODULES

    $(window).scroll(throttle(closeDrawer, 25));

    function kFormatter(num) {
      return num > 999 ? (num/1000).toFixed(1) + 'k' : num;
    }

    function getShares(){
      let request = new XMLHttpRequest()
      request.open('GET', 'https://graph.facebook.com/?id=' + thisUrl, true)
      request.onload = function() {
          let counter = $('#share-count')
          if (request.status >= 200 && request.status < 400) {
          // Success!
              let data = JSON.parse(request.responseText)
              let shares = data.share.share_count
              // this prevents occasional undefined errors in browsers such as Edge and IE10+
              if(data.share.share_count === undefined || 0) {
                counter.text('Share')
                console.log('Shares are null.')
              } else {
                  counter.text(kFormatter(shares));
              }
          } else {
          // We reached our target server, but it returned an error
              counter.text('')
          }
      }

      request.onerror = function() {
      // There was a connection error of some sort
      }

      request.send()
    }

    getShares();

  } // end constructor
}