// SET THIS TO YOUR LANGUAGE'S CONFIG
var RFEAPP = process.env.RFEAPP || 'ct';

const globalConfig = require("../src/html/data/global.json");
const globalConfigPath = "data/global.json";
const environment = process.env.NODE_ENV;
const ASSET_URL = environment === "development" ? '' : globalConfig.assetUrl;

module.exports = {
  html: true,
  images: true,
  fonts: true,
  static: true,
  svgSprite: false,
  ghPages: false,
  stylesheets: true,

  html: {
    htmlmin: false,
    dataFile: globalConfigPath,
    nunjucksRender: {
      manageEnv: function (env) {
        env.addGlobal("RFEAPP", RFEAPP);
        env.addGlobal("ASSET_URL", ASSET_URL);
        if (environment === "development") {
          env.addGlobal("production", false);
        }
        if (environment === "production") {
          env.addGlobal("production", true);
        }
      }
    }
  },

  javascripts: {
    entry: {
      // files paths are relative to
      // javascripts.dest in path-config.json
      app: ["./app.js"],
      head: ["./head.js"]
    },

    provide: {
      $: "jquery",
      jQuery: "jquery",
    },

    production: {
      uglifyJsPlugin: {
        extractComments: true
      },
      definePlugin: {
        PRODUCTION: JSON.stringify(true)
      }
    },

    development: {
      plugins: webpack => {
        return [
          new webpack.DefinePlugin({
            PRODUCTION: JSON.stringify(false)
          })
        ];
      }
    }
  },

  browserSync: {
    open: false,
    notify: false,
    server: {
      // should match `dest` in
      // path-config.json
      baseDir: "public"
    }
  },

  stylesheets: { cssnano: { zindex: false } },

  production: {
    rev: true
  }
};
